import numpy as np
import pandas as pd
from sklearn.feature_selection import SelectFromModel, SelectKBest, RFE, chi2

data = pd.read_csv("heart.csv")

train = data.copy()
target = train.pop('target')
train.head(2)

num_features_opt = 25   # the number of features that we need to choose as a result
num_features_max = 35   # the somewhat excessive number of features, which we will choose at each stage
features_best = []

lsvc = LinearSVC(C=0.1, penalty="l1", dual=False).fit(train, target)
model = SelectFromModel(lsvc, prefit=True)
X_new = model.transform(train)
X_selected_df = pd.DataFrame(X_new, columns=[train.columns[i] for i in range(len(train.columns)) if model.get_support()[i]])
features_best.append(X_selected_df.columns.tolist())