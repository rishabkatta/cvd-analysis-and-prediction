# cvd analysis and prediction

This is a study of different machine learning algorithms on how effective they are at predicting the presence of heart disease in a patient. The dataset used is UCI's Heart Disease dataset. This is the capstone project of Rishab Katta, RIT MS CS 2021.