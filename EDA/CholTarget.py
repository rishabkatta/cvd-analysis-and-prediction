import numpy as np
import pandas as pd
import pandas_profiling as pp
import math
import random
import seaborn as sns
import matplotlib.pyplot as plt

data = pd.read_csv("heart.csv")

plt.rcParams['figure.figsize'] = (12, 9)
sns.violinplot(data['target'], data['chol'], palette = 'Set2')
plt.title('Cholesterol levels Vs Target', fontsize = 20, fontweight = 30)
plt.show()