import numpy as np
import pandas as pd
import pandas_profiling as pp
import math
import random
import seaborn as sns
import matplotlib.pyplot as plt

data = pd.read_csv("heart.csv")

plt.rcParams['figure.figsize'] = (12, 9)
sns.boxplot(data['target'], data['trestbps'], palette = 'pastel')
plt.title('Blood Pressure Vs Target', fontsize = 20)
plt.show()