import numpy as np
import pandas as pd

metrics_all = {1 : 'r2_score', 2: 'acc', 3 : 'rmse', 4 : 're'}
metrics_now = [1, 2, 3, 4]

models = pd.DataFrame({
    'Model': ['Linear Regression', 'Support Vector Machines', 'Linear SVC',
              'MLP Classifier', 'Stochastic Gradient Decent',
              'Decision Tree Classifier', 'Random Forest Classifier',  'XGB Classifier', 'LGBM Classifier',
              'Gradient Boosting Classifier', 'RidgeCV', 'Bagging Classifier', 'ExtraTrees Classifier',
              'AdaBoost Classifier', 'Logistic Regression',
              'KNN', 'Naive Bayes', 'NN model', 'Gaussian Process Classification',
              'VotingClassifier']})


for x in metrics_now:
    xs = metrics_all[x]
    models[xs + '_train'] = acc_all[(x-1)*2]
    models[xs + '_test'] = acc_all[(x-1)*2+1]
    if xs == "acc":
        models[xs + '_diff'] = models[xs + '_train'] - models[xs + '_test']


print('Prediction accuracy for models')
ms = metrics_all[metrics_now[1]] # the first from metrics
models[['Model', ms+'_train', ms+'_test', 'acc_diff']].sort_values(by=[(ms + '_test'), (ms + '_train')], ascending=False)

pd.options.display.float_format = '{:,.2f}'.format

metrics_main = 2
xs = metrics_all[metrics_main]
xs_train = metrics_all[metrics_main] + '_train'
xs_test = metrics_all[metrics_main] + '_test'
print('The best models by the',xs,'criterion:')
direct_sort = False if (metrics_main >= 2) else True
models_sort = models.sort_values(by=[xs_test, xs_train], ascending=direct_sort)

models_best = models_sort[(models_sort.acc_diff < 10) & (models_sort.acc_test > 86)]
models_best[['Model', ms + '_train', ms + '_test']].sort_values(by=['acc_test'], ascending=False)

models_best_best = models_best[(models_best.acc_test > 90)]
models_best_best[['Model', ms + '_train', ms + '_test']].sort_values(by=['acc_test'], ascending=False)

