import numpy as np
import pandas as pd
import pandas_profiling as pp
from autoviz.AutoViz_Class import AutoViz_Class

# models


import warnings
warnings.filterwarnings("ignore")

data.to_csv('data_EDA.csv', index=False)

AV = AutoViz_Class()
data = pd.read_csv('./data_EDA.csv')
df = AV.AutoViz(filename="",sep=',', depVar='target', dfte=data, header=0, verbose=2, lowess=False,
                chart_format='svg',  max_cols_analyzed=30)

pp.ProfileReport(data[main_cols])